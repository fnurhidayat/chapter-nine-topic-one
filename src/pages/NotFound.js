import React, { Component } from 'react'
import {
  Container,
} from 'reactstrap'

class NotFound extends Component {
  render() {
    return (
      <Container className="p-4">
        <h1>404 Not Found!</h1>
      </Container>
    )
  }
}

export default NotFound
