import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Home from '../../pages/Home'
import About from '../../pages/About'
import Contact from '../../pages/Contact'
import NotFound from '../../pages/NotFound'
import Login from '../../pages/Login'

class App extends Component {
  render() {

    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/login" component={Login} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App
